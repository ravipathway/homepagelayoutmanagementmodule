﻿/*
' Copyright (c) 2017  Christoc.com
'  All rights reserved.
' 
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.
' 
*/

using System;
using System.Web.UI.WebControls;
using NRNA.Modules.HomePageLayoutManagementModule.Components;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;
using DotNetNuke.UI.Utilities;

namespace NRNA.Modules.HomePageLayoutManagementModule
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The View class displays the content
    /// 
    /// Typically your view control would be used to display content or functionality in your module.
    /// 
    /// View may be the only control you have in your project depending on the complexity of your module
    /// 
    /// Because the control inherits from HomePageLayoutManagementModuleModuleBase you have access to any custom properties
    /// defined there, as well as properties from DNN such as PortalId, ModuleId, TabId, UserId and many more.
    /// 
    /// </summary>
    /// -----------------------------------------------------------------------------
    public partial class View : HomePageLayoutManagementModuleModuleBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                var controlToLoad = "";

                if (Settings.Contains("DefaultView"))
                {
                    var defaultview = Settings["DefaultView"].ToString();
                    switch (defaultview)
                    {
                        case "0": controlToLoad = "Controls/TopView.ascx";break;
                        case "1": controlToLoad = "Controls/GridView.ascx"; break;
                        case "2": controlToLoad = "Controls/SingleImageView.ascx"; break;
                        case "3": controlToLoad = "Controls/MultipleImageView.ascx"; break;
                        case "4": controlToLoad = "Controls/TabView.ascx"; break;
                        case "5": controlToLoad = "Controls/HomePageLayoutForEdit.ascx"; break;
                        case "6": controlToLoad = "Controls/Edit.ascx"; break;
                        case "7": controlToLoad = "Controls/ContentView.ascx"; break;
                        case "8": controlToLoad = "Controls/LeftBannerView.ascx"; break;
                        case "9": controlToLoad = "Controls/RightBannerView.ascx"; break;
                    }
                }
                else
                {
                    controlToLoad = "Controls/ContentView.ascx";
                }
                var mbl = (HomePageLayoutManagementModuleModuleBase)LoadControl(controlToLoad);
                mbl.ModuleConfiguration = ModuleConfiguration;
                mbl.ID = System.IO.Path.GetFileNameWithoutExtension(controlToLoad);
                phViewControl.Controls.Add(mbl);

            }
            catch (Exception ex)
            {
                //throw;
            }
        }
    }
}