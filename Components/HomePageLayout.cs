﻿/*
' Copyright (c) 2017 Christoc.com
'  All rights reserved.
' 
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.
' 
*/

using System;
using System.Web.Caching;
using DotNetNuke.Common.Utilities;
using DotNetNuke.ComponentModel.DataAnnotations;
using DotNetNuke.Entities.Content;

namespace NRNA.Modules.HomePageLayoutManagementModule.Components
{
    [TableName("NRNAHomePageLayoutManagementModule")]
    //setup the primary key for table
    [PrimaryKey("ContentId", AutoIncrement = true)]
    //configure caching using PetaPoco
    [Cacheable("Items", CacheItemPriority.Default, 20)]
    //scope the objects to the ModuleId of a module on a page (or copy of a module on a page)
    [Scope("ModuleId")]
    public class HomePageLayout
    {
        ///<summary>
        /// The ID of your object with the name of the ItemName
        ///</summary>
        public int ContentId { get; set; }
       
        public string ContentTitle { get; set; }
      
        public string ContentSubTitle { get; set; }

        public DateTime ContentStartDate { get; set; }

        public DateTime ContentEndDate { get; set; }

        public int CategoryId { get; set; }

      //  public int GridViewPosition { get; set; }

        public string ContentImage { get; set; }

        public string  ContentImageUrl { get; set; }
        
        public string ContentButtonText1 { get; set; }

        public string ContentLink1 { get; set; }

        public string ContentButtonText2 { get; set; }

        public string ContentLink2 { get; set; }
     
        public string Country { get; set; }

        public int ShowInICCNCC { get; set; }

        public string PostedFromDomain { get; set; }
       
        public int ModuleId { get; set; }

        public int PortalId { get; set; }
       
        public int CreatedByUserId { get; set; }
        
        public int LastModifiedByUserId { get; set; }
        
        public DateTime CreatedOnDate { get; set; }

        public DateTime LastModifiedOnDate { get; set; }
    }
}
