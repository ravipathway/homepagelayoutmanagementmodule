﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NRNA.Modules.HomePageLayoutManagementModule.Components
{
    public class Fetch
    {
        public int ContentId { get; set; }

        public string ContentTitle { get; set; }

        public string ContentSubTitle { get; set; }

        public DateTime ContentStartDate { get; set; }

        public DateTime ContentEndDate { get; set; }

        public int CategoryId { get; set; }

        public string ContentImage { get; set; }

        public string ContentImageUrl { get; set; }

        public string ContentButtonText1 { get; set; }

        public string ContentLink1 { get; set; }

        public string ContentButtonText2 { get; set; }

        public string ContentLink2 { get; set; }

        public string Country { get; set; }

        public int ShowInICCNCC { get; set; }

        public string PostedFromDomain { get; set; }

        public int ModuleId { get; set; }

        public int PortalId { get; set; }

        public int CreatedByUserId { get; set; }

        public int LastModifiedByUserId { get; set; }

        public DateTime CreatedOnDate { get; set; }

        public DateTime LastModifiedOnDate { get; set; }

        public string CategoryName { get; set; }
    }
}