﻿/*
' Copyright (c) 2017 Christoc.com
'  All rights reserved.
' 
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.
' 
*/
using System.Collections.Generic;
using DotNetNuke.Data;
using System;

namespace NRNA.Modules.HomePageLayoutManagementModule.Components
{

    public class HomePageLayoutController
    {

        public void CreateFeatureItem(HomePageLayout t)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<HomePageLayout>();
                rep.Insert(t);
            }
        }


        public IEnumerable<HomePageLayout> GetFeatureItems(int moduleId)
        {
            IEnumerable<HomePageLayout> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<HomePageLayout>();
                t = rep.Get(moduleId);
            }
            return t;
        }






        public void CreateItem(HomePageLayout t)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<HomePageLayout>();
                rep.Insert(t);
            }
        }

        public void DeleteItem(int itemId, int moduleId)
        {
            var t = GetItem(itemId, moduleId);
            DeleteItem(t);
        }

        public void DeleteItem(HomePageLayout t)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<HomePageLayout>();
                rep.Delete(t);
            }
        }

        public IEnumerable<HomePageLayout> GetItems(int moduleId)
        {
            IEnumerable<HomePageLayout> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<HomePageLayout>();
                t = rep.Get(moduleId);
            }
            return t;
        }

        public HomePageLayout GetItem(int itemId, int moduleId)
        {
            HomePageLayout t;
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<HomePageLayout>();
                t = rep.GetById(itemId, moduleId);
            }
            return t;
        }

        public void UpdateItem(HomePageLayout t)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<HomePageLayout>();
                rep.Update(t);
            }
        }

        public HomePageLayout GetContent(int itemId, int moduleId)
        {
            HomePageLayout t;
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<HomePageLayout>();
                t = rep.GetById(itemId, moduleId);
            }
            return t;
        }

        public HomePageLayout GetContent(int itemId)
        {
            HomePageLayout t;
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<HomePageLayout>();
                t = rep.GetById(itemId);
            }
            return t;
        }

        public IEnumerable<HomePageLayout> GetContents(int itemId, int portalId)
        {
            IEnumerable<HomePageLayout> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                t = ctx.ExecuteQuery<HomePageLayout>(System.Data.CommandType.Text, @"SELECT * FROM [NRNAHomePageLayoutManagementModule] WHERE ContentId=@0 AND PortalId = @1", itemId, portalId);
            }
            return t;
        }

        public IEnumerable<HomePageLayout> GetContentsForICC(int portalId)
        {
            IEnumerable<HomePageLayout> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                t = ctx.ExecuteQuery<HomePageLayout>(System.Data.CommandType.Text, @"SELECT * FROM [NRNAHomePageLayoutManagementModule] WHERE (ShowInICCNCC = 2) OR PortalId = @0", portalId);
            }
            return t;
        }

        public IEnumerable<HomePageLayout> GetContentsForNCC(int portalId)
        {
            IEnumerable<HomePageLayout> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                t = ctx.ExecuteQuery<HomePageLayout>(System.Data.CommandType.Text, @"SELECT * FROM [NRNAHomePageLayoutManagementModule] WHERE portalId = @0", portalId);
            }
            return t;
        }

        public IEnumerable<HomePageLayout> GetTopViewContentsForICC(int portalId)
        {
            IEnumerable<HomePageLayout> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                t = ctx.ExecuteQuery<HomePageLayout>(System.Data.CommandType.Text, @"SELECT * FROM [NRNAHomePageLayoutManagementModule] T1
                    INNER JOIN [NRNAHomePageLayoutManagementCategory] T2 ON T1.CategoryId=T2.CategoryId WHERE ((T1.ShowInICCNCC = 2) OR T1.portalId = @0) AND (T2.CategoryName='Top View' OR T2.CategoryName='Top Banner') AND ContentEndDate >=(SELECT getdate())", portalId);
            }
            return t;
        }

        public IEnumerable<HomePageLayout> GetTopViewContentsForNCC(int portalId)
        {
            IEnumerable<HomePageLayout> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                t = ctx.ExecuteQuery<HomePageLayout>(System.Data.CommandType.Text, @"SELECT * FROM [NRNAHomePageLayoutManagementModule] T1
                    INNER JOIN [NRNAHomePageLayoutManagementCategory] T2 ON T1.CategoryId=T2.CategoryId WHERE ((T1.ShowInICCNCC = 3) OR T1.portalId = @0) AND (T2.CategoryName='Top View'  OR T2.CategoryName='Top Banner') AND ContentEndDate >=(SELECT getdate())", portalId);
            }
            return t;
        }

        public IEnumerable<HomePageLayout> GetRightBannerViewContentsForICC(int portalId)
        {
            IEnumerable<HomePageLayout> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                t = ctx.ExecuteQuery<HomePageLayout>(System.Data.CommandType.Text, @"SELECT * FROM [NRNAHomePageLayoutManagementModule] T1
                    INNER JOIN [NRNAHomePageLayoutManagementCategory] T2 ON T1.CategoryId=T2.CategoryId WHERE ((T1.ShowInICCNCC = 2) OR T1.portalId = @0) AND T2.CategoryName='Right Banner' AND ContentEndDate >=(SELECT getdate()) order by NEWID()", portalId);
            }
            return t;
        }

        public IEnumerable<HomePageLayout> GetRightBannerViewContentsForNCC(int portalId)
        {
            IEnumerable<HomePageLayout> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                t = ctx.ExecuteQuery<HomePageLayout>(System.Data.CommandType.Text, @"SELECT * FROM [NRNAHomePageLayoutManagementModule] T1
                    INNER JOIN [NRNAHomePageLayoutManagementCategory] T2 ON T1.CategoryId=T2.CategoryId WHERE  ((T1.ShowInICCNCC = 3) OR T1.portalId = @0) AND T2.CategoryName='Right Banner' AND ContentEndDate >=(SELECT getdate()) order by NEWID()", portalId);
            }
            return t;
        }

        public IEnumerable<HomePageLayout> GetLeftBannerViewContentsForICC(int portalId)
        {
            IEnumerable<HomePageLayout> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                t = ctx.ExecuteQuery<HomePageLayout>(System.Data.CommandType.Text, @"SELECT * FROM [NRNAHomePageLayoutManagementModule] T1
                    INNER JOIN [NRNAHomePageLayoutManagementCategory] T2 ON T1.CategoryId=T2.CategoryId WHERE ((T1.ShowInICCNCC = 2) OR T1.portalId = @0) AND T2.CategoryName='Left Banner' AND ContentEndDate >=(SELECT getdate()) order by NEWID()", portalId);
            }
            return t;
        }

        public IEnumerable<HomePageLayout> GetLeftBannerViewContentsForNCC(int portalId)
        {
            IEnumerable<HomePageLayout> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                t = ctx.ExecuteQuery<HomePageLayout>(System.Data.CommandType.Text, @"SELECT * FROM [NRNAHomePageLayoutManagementModule] T1
                    INNER JOIN [NRNAHomePageLayoutManagementCategory] T2 ON T1.CategoryId=T2.CategoryId WHERE ((T1.ShowInICCNCC = 3) OR T1.portalId = @0) AND T2.CategoryName='Left Banner' AND ContentEndDate >=(SELECT getdate()) order by NEWID()", portalId);
            }
            return t;
        }

        public void DeleteContent(int itemId)
        {
            var t = GetContent(itemId);
            DeleteContent(t);
        }

        public void DeleteContent(HomePageLayout t)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<HomePageLayout>();
                rep.Delete(t);
            }
        }

        public IEnumerable<Category> GetCategory(int itemId, int PortalId)
        {
            IEnumerable<Category> ts;
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<Category>();
                ts = rep.Get();
                return ts;
            }
        }

        public Category GetCategory(int itemId)
        {
            Category t;
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<Category>();
                t = rep.GetById(itemId);
            }
            return t;
        }

        public IEnumerable<Category> GetCategory()
        {
            IEnumerable<Category> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                t = ctx.ExecuteQuery<Category>(System.Data.CommandType.Text, @"SELECT * FROM NRNAHomePageLayoutManagementCategory");
            }
            return t;
        }

        public IEnumerable<HomePageLayout> GetTop7ContentForICC(int portalId)
        {
            IEnumerable<HomePageLayout> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                t = ctx.ExecuteQuery<HomePageLayout>(System.Data.CommandType.Text, @"SELECT * FROM [NRNAHomePageLayoutManagementModule] T1
                    INNER JOIN [NRNAHomePageLayoutManagementCategory] T2 ON T1.CategoryId=T2.CategoryId WHERE ((T1.ShowInICCNCC = 2) OR T1.portalId = @0) AND T2.CategoryName='Tab View'", portalId);
            }
            return t;
        }

        public IEnumerable<HomePageLayout> GetTop7ContentForNCC(int portalId)
        {
            IEnumerable<HomePageLayout> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                t = ctx.ExecuteQuery<HomePageLayout>(System.Data.CommandType.Text, @"SELECT * FROM [NRNAHomePageLayoutManagementModule] T1
                    INNER JOIN [NRNAHomePageLayoutManagementCategory] T2 ON T1.CategoryId=T2.CategoryId WHERE ((T1.ShowInICCNCC = 3) OR T1.portalId = @0) AND T2.CategoryName='Tab View'", portalId);
            }
            return t;
        }

        public IEnumerable<HomePageLayout> GetTop6GridViewContentForICC(int portalId)
        {
            IEnumerable<HomePageLayout> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                t = ctx.ExecuteQuery<HomePageLayout>(System.Data.CommandType.Text, @"SELECT * FROM [NRNAHomePageLayoutManagementModule] T1
                    INNER JOIN [NRNAHomePageLayoutManagementCategory] T2 ON T1.CategoryId=T2.CategoryId WHERE ((T1.ShowInICCNCC = 2) OR T1.portalId = @0) AND T2.CategoryName='Grid View'", portalId);
            }
            return t;
        }

        public void UpdateBeforeInsertInGridView(int gridposition, int portalId)
        {
            try
            {

                using (IDataContext ctx = DataContext.Instance())
                {
                    ctx.Execute(System.Data.CommandType.Text, @"update [NRNAHomePageLayoutManagementModule]
                        set GridViewPosition=0 where GridViewPosition=@0 AND PortalId=@1 AND CategoryId=2", gridposition, portalId);
                }

            }
            catch (Exception exc)
            {

                throw;
            }
        }

        public IEnumerable<HomePageLayout> GetTop6GridViewContentForNCC(int portalId)
        {
            IEnumerable<HomePageLayout> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                t = ctx.ExecuteQuery<HomePageLayout>(System.Data.CommandType.Text, @"SELECT * FROM [NRNAHomePageLayoutManagementModule] T1
                    INNER JOIN [NRNAHomePageLayoutManagementCategory] T2 ON T1.CategoryId=T2.CategoryId WHERE ((T1.ShowInICCNCC = 3) OR T1.portalId = @0) AND T2.CategoryName='Grid View'", portalId);
            }
            return t;
        }

        public IEnumerable<HomePageLayout> GetTop3SingleViewContentForICC(int portalId)
        {
            IEnumerable<HomePageLayout> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                t = ctx.ExecuteQuery<HomePageLayout>(System.Data.CommandType.Text, @"SELECT * FROM [NRNAHomePageLayoutManagementModule] T1
                    INNER JOIN [NRNAHomePageLayoutManagementCategory] T2 ON T1.CategoryId=T2.CategoryId WHERE ((T1.ShowInICCNCC = 2) OR T1.portalId = @0) AND T2.CategoryName='Single Image View'", portalId);
            }
            return t;
        }

        public IEnumerable<HomePageLayout> GetTop3SingleViewContentForNCC(int portalId)
        {
            IEnumerable<HomePageLayout> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                t = ctx.ExecuteQuery<HomePageLayout>(System.Data.CommandType.Text, @"SELECT * FROM [NRNAHomePageLayoutManagementModule] T1
                    INNER JOIN [NRNAHomePageLayoutManagementCategory] T2 ON T1.CategoryId=T2.CategoryId WHERE ((T1.ShowInICCNCC = 3) OR T1.portalId = @0) AND T2.CategoryName='Single Image View'", portalId);
            }
            return t;
        }

        public IEnumerable<HomePageLayout> GetTop6MultipleViewContentForICC(int portalId)
        {
            IEnumerable<HomePageLayout> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                t = ctx.ExecuteQuery<HomePageLayout>(System.Data.CommandType.Text, @"SELECT * FROM [NRNAHomePageLayoutManagementModule] T1
                    INNER JOIN [NRNAHomePageLayoutManagementCategory] T2 ON T1.CategoryId=T2.CategoryId WHERE ((T1.ShowInICCNCC = 2) OR T1.portalId = @0) AND T2.CategoryName='Multiple Image View'", portalId);
            }
            return t;
        }

        public IEnumerable<HomePageLayout> GetTop6MultipleViewContentForNCC(int portalId)
        {
            IEnumerable<HomePageLayout> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                t = ctx.ExecuteQuery<HomePageLayout>(System.Data.CommandType.Text, @"SELECT * FROM [NRNAHomePageLayoutManagementModule] T1
                    INNER JOIN [NRNAHomePageLayoutManagementCategory] T2 ON T1.CategoryId=T2.CategoryId WHERE ((T1.ShowInICCNCC = 2) OR T1.portalId = @0) AND T2.CategoryName='Multiple Image View'", portalId);
            }
            return t;
        }

        public int GetTotalItemCountForICC(int portalId)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                return ctx.ExecuteScalar<int>(System.Data.CommandType.Text, "SELECT COUNT(ContentId) FROM [NRNAHomePageLayoutManagementModule] WHERE  (ShowInICCNCC = 2 ) OR PortalId = @0", portalId);
            }
        }

        public int GetTotalItemCountForNCC(int portalId)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                return ctx.ExecuteScalar<int>(System.Data.CommandType.Text, "SELECT COUNT(ContentId) FROM [NRNAHomePageLayoutManagementModule] WHERE  (ShowInICCNCC = 3)  OR PortalId = @0", portalId);
            }
        }

        public IEnumerable<HomePageLayout> GetContentsForICC(int portalId, int PageIndex, int PageSize)
        {
            IEnumerable<HomePageLayout> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<HomePageLayout>();
                t = ctx.ExecuteQuery<HomePageLayout>(System.Data.CommandType.Text, @"SELECT * FROM [NRNAHomePageLayoutManagementModule] T1
                    INNER JOIN [NRNAHomePageLayoutManagementCategory] T2 ON T1.CategoryId=T2.CategoryId WHERE ((T1.ShowInICCNCC = 2) OR T1.portalId = @0) AND T2.CategoryName='Tab View'", portalId);



                //t = rep.Find(PageIndex, PageSize, "WHERE ((ShowInICCNCC = 2) OR PortalId= @0) AND CategoryName='Tab View'", portalId);

                //t = ctx.ExecuteQuery<HomePageLayout>(PageIndex, PageSize, System.Data.CommandType.Text, @"SELECT * FROM [NRNAHomePageLayoutManagementModule] WHERE (ShowInICCNCC = 2) OR portalId = @0", portalId);
            }
            return t;
        }

        public IEnumerable<HomePageLayout> GetContentsForNCC(int portalId, int PageIndex, int PageSize)
        {
            IEnumerable<HomePageLayout> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<HomePageLayout>();
                t = ctx.ExecuteQuery<HomePageLayout>(System.Data.CommandType.Text, @"SELECT * FROM [NRNAHomePageLayoutManagementModule] T1
                    INNER JOIN [NRNAHomePageLayoutManagementCategory] T2 ON T1.CategoryId=T2.CategoryId WHERE ((T1.ShowInICCNCC = 3) OR T1.portalId = @0) AND T2.CategoryName='Tab View'", portalId);
                //t = rep.Find(PageIndex, PageSize, "WHERE ((ShowInICCNCC = 3) OR PortalId= @0) AND Category='Tab View'", portalId);
                //t = ctx.ExecuteQuery<HomePageLayout>(PageIndex, PageSize, System.Data.CommandType.Text, @"SELECT * FROM [NRNAHomePageLayoutManagementCategory] WHERE (ShowInICCNCC = 3) OR portalId = @0", portalId);
            }
            return t;
        }

        public IEnumerable<HomePageLayout> GetSearchedContent(string keyword, int portalId)
        {
            IEnumerable<HomePageLayout> t;
            try
            {
                string query = string.Format(@"select t.ContentId,t.ContentTitle,t.ContentSubTitle,t.ContentStartDate,t.ContentEndDate,t.CategoryId,t.ContentImage,t.ContentImageUrl,t.ContentButtonText1,
                                                  t.ContentLink1,t.ContentButtonText2,t.ContentLink2,t.ShowInICCNCC,t.Country,t.PortalID,t.PostedFromDomain,t.ModuleId,t.CreatedOnDate,t.CreatedByUserId,
                                                      t.LastModifiedOnDate,t.LastModifiedByUserId from 
                                                    (
                                                    select *,
                                                    (select CategoryName from NRNAHomePageLayoutManagementCategory where CategoryId = a.CategoryId) CategoryName
                                                        from NRNAHomePageLayoutManagementModule a
                                                    ) as t
                                                    WHERE 
    (ContentTitle like N'%{0}%' or ContentSubTitle like N'%{0}%' or CategoryName like N'%{0}%') AND PortalId={1}", keyword, portalId);
                using (IDataContext ctx = DataContext.Instance())
                {
                    var rep = ctx.GetRepository<HomePageLayout>();

                    t = ctx.ExecuteQuery<HomePageLayout>(System.Data.CommandType.Text, query);
                    //, keyword, portalId");

                    //    t = rep.Find(query + "and PortalId=@0 ", portalId);
                    //t = rep.Find("WHERE NewsTitle like '%" + search + "%' or NewsSubTitle like N'%" + search + "%' or NewsShortDescription like N'%" + search + "%'  or NewsDate between " + FromDate + "and" + ToDate);

                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return t;
        }
        public IEnumerable<HomePageLayout> GetHomePageItemsforAdmin(int portalId, string sortcolumn, string directionType)
        {
            try
            {
                IEnumerable<HomePageLayout> t;
                using (IDataContext ctx = DataContext.Instance())
                {
                    var rep = ctx.GetRepository<HomePageLayout>();

                    t = ctx.ExecuteQuery<HomePageLayout>(System.Data.CommandType.Text, @"SELECT * FROM [NRNAHomePageLayoutManagementModule] WHERE [PortalId]=" + portalId + "ORDER BY " + sortcolumn + " " + directionType + " ");

                }
                return t;
            }
            catch (Exception exc)
            {

                throw exc;
            }
        }

    }
}
