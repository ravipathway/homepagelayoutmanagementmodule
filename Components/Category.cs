﻿using DotNetNuke.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace NRNA.Modules.HomePageLayoutManagementModule.Components
{
    public class Category
    {
        [TableName("NRNAHomePageLayoutManagementCategory")]
        //setup the primary key for table
        [PrimaryKey("CategoryId", AutoIncrement = true)]
        //configure caching using PetaPoco
        [Cacheable("Items", CacheItemPriority.Default, 20)]
        //scope the objects to the ModuleId of a module on a page (or copy of a module on a page)
        [Scope("ModuleId")]
        public int CategoryId { get; set; }

        public string  CategoryName { get; set; }

        public int PortalId { get; set; }
    }
}