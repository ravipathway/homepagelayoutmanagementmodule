﻿/*
' Copyright (c) 2017  Christoc.com
'  All rights reserved.
' 
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.
' 
*/

using System;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.FileSystem;
using System.Text;
using DotNetNuke.Common;
using DotNetNuke.Data;

namespace NRNA.Modules.HomePageLayoutManagementModule
{
    public class HomePageLayoutManagementModuleModuleBase : PortalModuleBase
    {
        public int ContentId
        {
            get
            {
                var qs = Request.QueryString["cid"];
                if (qs != null)
                    return Convert.ToInt32(qs);
                return -1;
            }

        }

        public int PageNumber
        {
            get
            {
                if (Request.QueryString["p"] == null)
                    return 1;
                else
                    return int.Parse(Request.QueryString["p"]);
            }
        }

        public int PageSize
        {
            get
            {
                if (Request.QueryString["psize"] == null)
                    return 5;
                else
                    return int.Parse(Request.QueryString["psize"]);
            }
        }

        public void BuildPageList(int totalItems, System.Web.UI.WebControls.Literal litPagingControl)
        {
            float numberOfPages = totalItems / (float)PageSize;
            int intNumberOfPages = Convert.ToInt32(numberOfPages);
            if (numberOfPages > intNumberOfPages)
            {
                intNumberOfPages++;
            }

            if (intNumberOfPages > 1)
            {
                litPagingControl.Text = "<ul class='pagination'>";
                if (PageNumber > 1)
                {
                    litPagingControl.Text += "<li><a href ='" + Globals.NavigateURL(this.TabId, string.Empty, new string[] { "p=" + (PageNumber - 1).ToString() }) + "' class='prev'><span class='icon icon-arrow-left'></span></a></li>";
                }
                for (int p = 1; p <= intNumberOfPages; p++)
                {

                    if (p == PageNumber)
                    {
                        litPagingControl.Text += "<li><strong>" + p.ToString() + "</strong></li>";
                    }
                    else
                    {
                        litPagingControl.Text += "<li><a href='" + Globals.NavigateURL(this.TabId, string.Empty, new string[] { "p=" + p.ToString() }) + "'>" + p.ToString() + "</a></li>";
                    }

                }
                if (PageNumber < intNumberOfPages)
                {
                    litPagingControl.Text += "<li><a href ='" + Globals.NavigateURL(this.TabId, string.Empty, new string[] { "p=" + (PageNumber + 1).ToString() }) + "' class='prev'><span class='icon icon-arrow-right'></span></a></li>";
                }
                litPagingControl.Text += "</ul>";
            }
        }

        public string GetImageFromImageID(string fileId)
        {
            if (fileId == "-1")
            {
                fileId = "3245";
            }
            StringBuilder sb = new StringBuilder("/Portals/");
            try
            {
                IFileInfo fi = FileManager.Instance.GetFile(Convert.ToInt32(fileId));
                sb.Append(fi.PortalId);
                sb.Append("/");
                sb.Append(fi.RelativePath);
            }
            catch (Exception ex)
            {
                return "";
            }
            return sb.ToString();
        }

        public bool ShowSecondButton(string ButtonText)
        {
            if (ButtonText.Trim().Length > 0)
                return true;
            else
                return false;
        }

        public bool ShowFirstButton(string buttonText)
        {
            if (buttonText.Trim().Length > 0)
                return true;
            else
                return false;
        }

        public string GetFormatedDate(string Startdatetime, string Enddatetime)
        {
            if (Startdatetime == Enddatetime)
            {
                return Convert.ToDateTime(Startdatetime).ToString("dd MMM yyyy");
            }
            else
            {
                return Convert.ToDateTime(Startdatetime).ToString("dd MMM yyyy") + " - " + Convert.ToDateTime(Enddatetime).ToString("dd MMM yyyy");
            }
        }

        public string GetTrimCharacter(string sWord)
        {
            if (sWord.Trim() == "")
            {
                return "";
            }
            return sWord.Substring(0, 100);
        }

        public string GetCategoryByCategoryId(string categoryId)
        {
            string t;
            using (IDataContext ctx = DataContext.Instance())
            {
                t = ctx.ExecuteScalar<string>(System.Data.CommandType.Text, @"SELECT CategoryName FROM NRNAHomePageLayoutManagementCategory WHERE CategoryId=@0 ", categoryId);
            }
            return t;

        }
    }
}