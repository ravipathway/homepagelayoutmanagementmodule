﻿using DotNetNuke.Services.Exceptions;
using NRNA.Modules.HomePageLayoutManagementModule.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NRNA.Modules.HomePageLayoutManagementModule.Controls
{
    public partial class Edit : HomePageLayoutManagementModuleModuleBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Implement your edit logic for your module
                if (!Page.IsPostBack)
                {

                    //check if we have an ID passed in via a querystring parameter, if so, load that item to edit.
                    //ItemId is defined in the ItemModuleBase.cs file
                    if (ContentId > 0)
                    {
                        var tc = new HomePageLayoutController();

                        var t = tc.GetContent(ContentId, PortalId);
                        if (t != null)
                        {
                            txtTitle.Text = t.ContentTitle;
                            txtSubTitle.Text = t.ContentSubTitle;
                            mainImage.FileID = Convert.ToInt32(t.ContentImage);
                            txtStartDate.SelectedDate = Convert.ToDateTime(t.ContentStartDate.ToString());
                            txtEndDate.SelectedDate = Convert.ToDateTime(t.ContentEndDate.ToString());
                            txtBtn1.Text = t.ContentButtonText1;
                            txtLink1.Text = t.ContentLink1;
                            txtBtn2.Text = t.ContentButtonText2;
                            txtLink2.Text = t.ContentLink2;
                            if (t.ShowInICCNCC == 1)
                            {
                                chkToAllNCC.Checked=true;
                            }
                            if(t.ShowInICCNCC==2)
                            {
                                chkRequest.Checked = true;
                            }
                        }
                    }

                    pnlICC.Visible = pnlNCC.Visible = false;

                    if (PortalSettings.UserInfo.IsSuperUser)
                    {
                        pnlICC.Visible = true;
                    }
                    else
                    {
                        pnlNCC.Visible = true;
                    }
                }
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }
    }
}