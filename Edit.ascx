﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Edit.ascx.cs" Inherits="NRNA.Modules.HomePageLayoutManagementModule.Controls.Edit" %>
<%@ Register TagPrefix="dnn" TagName="Label" Src="~/controls/LabelControl.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TextEditor" Src="~/controls/TextEditor.ascx" %>
<%@ Register TagPrefix="dnn" TagName="FilePickerUploader" Src="~/controls/FilePickerUploader.ascx" %>
<%@ Register TagPrefix="dnnui" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<style>
    .nrna .page-article .table {
        width: 100%;
        border: none;
        border-collapse: collapse;
    }

    .nrna .page-article th, td {
        padding: 15px;
        text-align: left;
        border: none;
    }

    .nrna .page-article input {
        width: 100%;
        float: left;
        text-align: left;
    }

    .nrna .page-article table tbody tr:nth-child(even) {
        background: #fff;
    }
</style>
<h2>Add/Edit Home Page Content And Add Banner</h2>
<table class="table">
    <tr>
        <td style="vertical-align: top; width: 200px;">Title:
            <br />
            (100 character)</td>
        <td>
            <asp:TextBox ID="txtTitle" runat="server" MaxLength="100" Width="200px" />
            <asp:RequiredFieldValidator runat="server" ID="rfvFCTitle" ControlToValidate="txtTitle" ErrorMessage="Title is required"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td style="vertical-align: top;">Sub Title:
            <br />
            (100 character) </td>
        <td>
            <asp:TextBox ID="txtSubTitle" runat="server" MaxLength="100"  Width="200px"/><br />
        </td>
    </tr>
    <tr>
        <td style="vertical-align: top;">Display from:</td>
        <td style="padding: 0px; margin: 0px;">
            <table>
                <tr>
                    <td style="border: none">
                        <dnnui:RadDatePicker ID="txtStartDate" Width="100px" runat="server" AutoPostBackControl="None">
                        </dnnui:RadDatePicker>
                    <td style="border: none">to:</td>
                    <td style="border: none">
                        <dnnui:RadDatePicker ID="txtEndDate" Width="100px" runat="server" AutoPostBackControl="None">
                        </dnnui:RadDatePicker>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>Category:</td>
        <td>
            <asp:DropDownList runat="server" ID="ddlCategory" Width="205px"></asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvCategory" runat="server" ControlToValidate="ddlCategory"
                CssClass="NormalRed" ErrorMessage=" Category is required" />
        </td>
    </tr>
    <tr>
        <td style="vertical-align: top;">Upload Image:
            <br />
            (1132px X 200px)</td>
        <td>
            <dnn:FilePickerUploader ID="mainImage" FileFilter="jpg,png,gif,jpeg" runat="server" />
        </td>
    </tr>
     <tr>
        <td>Image Link:</td>
        <td>
            <asp:TextBox ID="txtImageLink" runat="server" Width="200px"/>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtImageLink" ErrorMessage="URL for Image is required"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>First Button Text:</td>
        <td>
            <asp:TextBox ID="txtBtn1" runat="server" Width="200px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>Button Link:</td>
        <td>
            <asp:TextBox ID="txtLink1" runat="server" Width="200px"/>
        </td>
    </tr>
    <tr>
        <td>Second Button Text</td>
        <td>
            <asp:TextBox ID="txtBtn2" runat="server" Width="200px"/><br />
        </td>

    </tr>
    <tr>
        <td>Second Button Link</td>
        <td>
            <asp:TextBox ID="txtLink2" runat="server" Width="200px"/><br />
        </td>
    </tr>
    <asp:Panel runat="server" ID="pnlNCC" Visible="false">
        <tr>
            <td>Request to List in ICC:</td>
            <td>

                <asp:CheckBox ID="chkRequest" runat="server" Height="30px" Width="30px"/>

                <br />
            </td>
        </tr>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlICC" Visible="false">
        <tr>
            <td>List this in all NCC:</td>
            <td>

                <asp:CheckBox ID="chkToAllNCC" runat="server" Height="30px" Width="30px"/>

                <br />
            </td>
        </tr>
    </asp:Panel>
    <tr>
        <td></td>
        <td>
            <asp:LinkButton ID="btnSubmit" runat="server"
                OnClick="btnSubmit_Click" Text="Save" CssClass="button" />
            <asp:LinkButton ID="btnCancel" runat="server"
                OnClick="btnCancel_Click" Text="cancel" CssClass="button" />
        </td>
    </tr>
</table>

<script type="text/javascript">
    var isTimeSet = false;
    function timeSelecting(sender, args) {
        isTimeSet = true;
    }
    function dateSelected(sender, args) {
        if (args.get_oldDate() == null && !isTimeSet) {
            args.set_cancel(true);
            isTimeSet = true;
            sender.get_timeView().setTime(8, 30, 0, 0);
        }
        if (isTimeSet) {
            isTimeSet = false;
        }
    }
</script>
