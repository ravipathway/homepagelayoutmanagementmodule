﻿using DotNetNuke.Services.Exceptions;
using NRNA.Modules.HomePageLayoutManagementModule.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NRNA.Modules.HomePageLayoutManagementModule.Controls
{
    public partial class ContentView : HomePageLayoutManagementModuleModuleBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int totalRecords = 0;
                var tc = new HomePageLayoutController();
                if (PortalId == 0)
                {
                    rptItemList.DataSource = tc.GetContentsForICC(PortalId, PageNumber-1, PageSize);
                    totalRecords = tc.GetTotalItemCountForICC(PortalId);
                }
                else
                {
                    rptItemList.DataSource = tc.GetContentsForNCC(PortalId, PageNumber - 1, PageSize);
                    totalRecords = tc.GetTotalItemCountForICC(PortalId);
                }
                rptItemList.DataBind();
                //BuildPageList(totalRecords, litPaging);
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }
    }
}