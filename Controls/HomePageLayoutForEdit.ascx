﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HomePageLayoutForEdit.ascx.cs" Inherits="NRNA.Modules.HomePageLayoutManagementModule.Controls.HomePageLayoutForEdit" %>


<asp:Panel runat="server" ID="pnlMsg">
    <div>
        <asp:Literal runat="server" ID="litMessage"></asp:Literal>
    </div>
</asp:Panel>


<h2>Manage Home Page Layout</h2>
<asp:HyperLink ID="lnkEdit" runat="server" CssClass="button button-blue" Text="Add Content"
    Visible="true" Enabled="true" NavigateUrl="~/add-layout-content" Target="_blank" />

<hr />



<h6 style="text-decoration: underline">Filter By:</h6>

<%--<input type="text" placeholder="Find" id="txtSearch" />--%>
<div class="lead">

    <div>


        <asp:TextBox runat="server" ID="txtSearch" Width="350px" Height="30px" PlaceHolder="Search By Title / Sub-Title / Category"></asp:TextBox>
        &nbsp&nbsp&nbsp
        <asp:Button runat="server" ID="btnSubmit" Text="Search" OnClick="Search" CssClass="button  button-green" />


    </div>
</div>

<hr />


<h6 style="text-decoration: underline">List Of Notices </h6>

<div class="table-responsive">
    <asp:GridView runat="server" ID="gvFC" AllowCustomPaging="False" AllowPaging="True" AllowSorting="True"
        PageSize="10" DataKeyNames="ContentId" OnRowDeleting="gvFC_RowDeleting" AutoGenerateColumns="False"
        OnRowDataBound="gvFC_RowDataBound" OnRowCommand="gvFC_RowCommand" OnPageIndexChanging="gvFC_PageIndexChanging"
        CssClass="table"
        EmptyDataText="No records Found"
        CurrentSortField="ContentTitle" CurrentSortDirection="DESC" EnableSortingAndPagingCallbacks="false" OnSorting="gv_Sorting">
        <Columns>
           <%-- <asp:TemplateField HeaderText="SNo." ItemStyle-Width="5">
                <ItemTemplate>
                    <asp:TextBox ID="lblRowNumber" Text='<%#DataBinder.Eval(Container.DataItem,"ContentId").ToString()%>' runat="server"
                        BackColor="transparent" width="50px"/>
                </ItemTemplate>
            </asp:TemplateField>--%>
            <asp:TemplateField HeaderText="Image">
                <ItemTemplate>
                    <asp:HyperLink runat="server" Target="_blank" NavigateUrl='<%# GetImageFromImageID(DataBinder.Eval(Container.DataItem,"ContentImage").ToString()) %>'>
                <div class="img-holder">
                <asp:Image runat="server" ImageUrl='<%# GetImageFromImageID(DataBinder.Eval(Container.DataItem,"ContentImage").ToString()) %>' AlternateText="Click on image to view original image" />
                </div>
                    </asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Title, SubTitle and Buttons" SortExpression="ContentTitle">
                <ItemTemplate>
                    <strong>
                        <asp:Literal runat="server" ID="lblTitle" Text='<%# DataBinder.Eval(Container.DataItem,"ContentTitle").ToString() %>'></asp:Literal>
                    </strong>
                    <p>
                        <asp:Literal runat="server" ID="Literal1" Text='<%# DataBinder.Eval(Container.DataItem,"ContentSubTitle").ToString() %>'></asp:Literal>
                    </p>
                    <asp:HyperLink runat="server" ID="lnk1" CssClass="button button-donate mrg-xs-t"
                        NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"ContentLink1").ToString() %>'
                        Visible='<%# ShowFirstButton(DataBinder.Eval(Container.DataItem,"ContentButtonText1").ToString()) %>'>
                        <asp:Literal runat="server" ID="lit1" Text='<%# DataBinder.Eval(Container.DataItem,"ContentButtonText1").ToString() %>'
                            Visible='<%# ShowFirstButton(DataBinder.Eval(Container.DataItem,"ContentButtonText1").ToString()) %>'></asp:Literal>
                    </asp:HyperLink>
                    &nbsp; &nbsp;
                    <asp:HyperLink runat="server" ID="lnk2" CssClass="button button-donate" Visible='<%# ShowSecondButton(DataBinder.Eval(Container.DataItem,"ContentButtonText2").ToString()) %>'
                        NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"ContentLink2").ToString() %>'>
                        <asp:Literal runat="server" ID="lit2" Visible='<%# ShowSecondButton(DataBinder.Eval(Container.DataItem,"ContentButtonText2").ToString()) %>'
                            Text='<%# DataBinder.Eval(Container.DataItem,"ContentButtonText2").ToString() %>'></asp:Literal>
                    </asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Category">
                <ItemTemplate>
                    <asp:Literal Text='<%# GetCategoryByCategoryId(DataBinder.Eval(Container.DataItem,"CategoryId").ToString()) %>' runat="server"></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
           <%-- <asp:TemplateField HeaderText="order by">
                <ItemTemplate>
                    <asp:TextBox Text="SN" runat="server"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>--%>

            <asp:TemplateField HeaderText="Manage" ItemStyle-Width="21%">
                <ItemTemplate>
                    <asp:HyperLink ID="lnkEdit" runat="server" CssClass="button" Text="Edit" Visible="true"
                        Enabled="true" Target="_blank" NavigateUrl='<%# "~/add-layout-content/cid/" + DataBinder.Eval(Container.DataItem,"ContentID").ToString() %>' />
                    <asp:LinkButton ID="lnkDelete" runat="server" CssClass="button  button-red" Text="Delete"
                        Visible="true" Enabled="true" CommandName="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</div>


<%--<asp:GridView runat="server" ID="gvFC" AllowCustomPaging="False" AllowPaging="True" PageSize="10" DataKeyNames="ContentID" OnRowDeleting="gvFC_RowDeleting"
    AutoGenerateColumns="False" OnRowDataBound="gvFC_RowDataBound" OnRowCommand="gvFC_RowCommand" OnPageIndexChanging="gvFC_PageIndexChanging">
    <Columns>
        <asp:TemplateField HeaderText="SNo." ItemStyle-Width="5">
            <ItemTemplate>
                <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" BackColor="transparent" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Image">
            <ItemTemplate>
                <asp:HyperLink runat="server" Target="_blank" NavigateUrl='<%# GetImageFromImageID(DataBinder.Eval(Container.DataItem,"ContentImage").ToString()) %>'>
                <asp:Image runat="server" Width="200px" ImageUrl='<%# GetImageFromImageID(DataBinder.Eval(Container.DataItem,"ContentImage").ToString()) %>' AlternateText="Click on image to view original image" />
                </asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Title, SubTitle and Buttons">
            <ItemTemplate>
                <h3>
                    <asp:Literal runat="server" ID="lblTitle" Text='<%# DataBinder.Eval(Container.DataItem,"ContentTitle").ToString() %>'></asp:Literal></h3>
                <p>
                    <asp:Literal runat="server" ID="Literal1" Text='<%# DataBinder.Eval(Container.DataItem,"ContentSubTitle").ToString() %>'></asp:Literal>
                </p>

                <asp:HyperLink runat="server" ID="lnk1" CssClass="button button-donate" NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"ContentLink1").ToString() %>'
                    Visible='<%# ShowFirstButton(DataBinder.Eval(Container.DataItem,"ContentButtonText1").ToString()) %>'>
                    <asp:Literal runat="server" ID="lit1" Text='<%# DataBinder.Eval(Container.DataItem,"ContentButtonText1").ToString() %>'
                        Visible='<%# ShowFirstButton(DataBinder.Eval(Container.DataItem,"ContentButtonText1").ToString()) %>'></asp:Literal>
                </asp:HyperLink>
                &nbsp; &nbsp;
                <asp:HyperLink runat="server" ID="lnk2" CssClass="button button-donate"
                    Visible='<%# ShowSecondButton(DataBinder.Eval(Container.DataItem,"ContentButtonText2").ToString()) %>'
                    NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"ContentLink2").ToString() %>'>
                    <asp:Literal runat="server" ID="lit2"
                        Visible='<%# ShowSecondButton(DataBinder.Eval(Container.DataItem,"ContentButtonText2").ToString()) %>'
                        Text='<%# DataBinder.Eval(Container.DataItem,"ContentButtonText2").ToString() %>'></asp:Literal>
                </asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Manage">
            <ItemTemplate>
                <asp:HyperLink ID="lnkEdit" runat="server" CssClass="button button-green" Text="Edit" Visible="true" Enabled="true"
                    NavigateUrl='<%# "~/add-layout-content/cid/" + DataBinder.Eval(Container.DataItem,"ContentID").ToString() %>' />
                <br />
                <br />
                <asp:LinkButton ID="lnkDelete" runat="server" CssClass="button  button-donate" Text="Delete" Visible="true" Enabled="true" CommandName="Delete" />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>--%>
