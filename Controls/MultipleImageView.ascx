﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MultipleImageView.ascx.cs" Inherits="NRNA.Modules.HomePageLayoutManagementModule.Controls.MultipleImageView" %>

<h2 class="h3">What else we are doing</h2>
<div class="service-holder">
    <asp:Repeater ID="rptContent" runat="server">
        <HeaderTemplate>
            <ul class="service-list">
        </HeaderTemplate>
        <ItemTemplate>
            <div class="image">
                <asp:HyperLink runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"ContentImageUrl").ToString() %>'>
                    <asp:Image runat="server" Style="max-height: 600px; max-width: 1110px"
                        ImageUrl='<%# GetImageFromImageID(DataBinder.Eval(Container.DataItem,"ContentImage").ToString()) %>' />
                </asp:HyperLink>
            </div>
            <asp:Literal runat="server" ID="lblTitle" Text='<%# DataBinder.Eval(Container.DataItem,"ContentTitle").ToString() %>'></asp:Literal>
        </ItemTemplate>
        <FooterTemplate>
            </ul>      
        </FooterTemplate>
    </asp:Repeater>
</div>
