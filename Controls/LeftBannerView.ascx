﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeftBannerView.ascx.cs" Inherits="NRNA.Modules.HomePageLayoutManagementModule.Controls.LeftBannerView" %>
<div class="slideset">
    <asp:Repeater ID="Repeater1" runat="server">
        <ItemTemplate>
            <div class="slide" >
                <a href='<%# DataBinder.Eval(Container.DataItem,"ContentImageUrl").ToString() %>'>
                    <img src='<%# GetImageFromImageID(DataBinder.Eval(Container.DataItem,"ContentImage").ToString()) %>' style="height:90px; width:655px">
                </a>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>