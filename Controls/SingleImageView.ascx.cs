﻿using DotNetNuke.Services.Exceptions;
using NRNA.Modules.HomePageLayoutManagementModule.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NRNA.Modules.HomePageLayoutManagementModule.Controls
{
    public partial class SingleImageView : HomePageLayoutManagementModuleModuleBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var tc = new HomePageLayoutController();
                if (PortalId == 0)
                    rptContent.DataSource = tc.GetTop3SingleViewContentForICC(PortalId);
                else
                    rptContent.DataSource = tc.GetTop3SingleViewContentForNCC(PortalId);
                rptContent.DataBind();
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }
    }
}