﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopView.ascx.cs" Inherits="NRNA.Modules.HomePageLayoutManagementModule.Controls.TopView" %>
<div class="slideset">
    <asp:Repeater ID="rptContent" runat="server">
        <ItemTemplate>
            <div class="slide">
                <div class="image">
                    <asp:HyperLink runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"ContentImageUrl").ToString() %>'>
                <asp:Image runat="server"
                    ImageUrl='<%# GetImageFromImageID(DataBinder.Eval(Container.DataItem,"ContentImage").ToString()) %>' />
                    </asp:HyperLink>
                </div>
                <div class="overlay">
                    <h3>
                        <asp:Literal runat="server" ID="lblTitle" Text='<%# DataBinder.Eval(Container.DataItem,"ContentTitle").ToString() %>'></asp:Literal>
                    </h3>
                    <asp:HyperLink runat="server" ID="lnk1" CssClass="button button-donate" NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"ContentLink1").ToString() %>'
                        Visible='<%# ShowFirstButton(DataBinder.Eval(Container.DataItem,"ContentButtonText1").ToString()) %>'>
                        <asp:Literal runat="server" ID="lit1" Text='<%# DataBinder.Eval(Container.DataItem,"ContentButtonText1").ToString() %>'
                            Visible='<%# ShowFirstButton(DataBinder.Eval(Container.DataItem,"ContentButtonText1").ToString()) %>'></asp:Literal>
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="lnk2" CssClass="button button-donate"
                        Visible='<%# ShowSecondButton(DataBinder.Eval(Container.DataItem,"ContentButtonText2").ToString()) %>'
                        NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"ContentLink2").ToString() %>'>
                        <asp:Literal runat="server" ID="lit2" Visible='<%# ShowSecondButton(DataBinder.Eval(Container.DataItem,"ContentButtonText2").ToString()) %>'
                            Text='<%# DataBinder.Eval(Container.DataItem,"ContentButtonText2").ToString() %>'></asp:Literal>
                    </asp:HyperLink>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>
