﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TabView.ascx.cs" Inherits="NRNA.Modules.HomePageLayoutManagementModule.Controls.TabView" %>

<div class="skin clearfix">
    <div id="parentVerticalTab" class="clearfix">
        <div class="heading-mini heading-mini-2">
            <h2 class="text">Latest Projects</h2>
        </div>
        <div class="resp-tabs-container hor_1">
            <asp:Repeater runat="server" ID="rptItemList">
                <ItemTemplate>
                    <div>
                        <a href='<%# DataBinder.Eval(Container.DataItem,"ContentImageUrl").ToString() %>'
                            target="_blank">
                            <img src='<%# GetImageFromImageID(DataBinder.Eval(Container.DataItem,"ContentImage").ToString()) %>'
                                alt="">
                            <p class="dtls">
                                <asp:Literal ID="Literal1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ContentSubTitle").ToString() %>'></asp:Literal>
                            </p>
                        </a>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <asp:Repeater ID="rptItemList2" runat="server">
            <HeaderTemplate>
                <ul class="resp-tabs-list hor_1">
            </HeaderTemplate>
            <ItemTemplate>
                <li>
                    <%# DataBinder.Eval(Container.DataItem, "ContentTitle").ToString() %>
                </li>
            </ItemTemplate>
            <FooterTemplate>
                </ul>
            </FooterTemplate>
        </asp:Repeater>
        <asp:HyperLink CssClass="link fly-b-r" runat="server" ID="lnkContent" NavigateUrl="~/home-page-contents"
            Text="View All"></asp:HyperLink>
    </div>
</div>
