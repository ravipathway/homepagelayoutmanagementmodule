﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SingleImageView.ascx.cs" Inherits="NRNA.Modules.HomePageLayoutManagementModule.Controls.SingleImageView" %>
<div class="heading-mini heading-mini-1">
    <h2 class="text">Latest Projects</h2>
</div>
<div class="slideshow latest-projects">
    <div class="slideset">
        <asp:Repeater ID="rptContent" runat="server">
            <ItemTemplate>
                <div class="slide">
                    <a href='<%# DataBinder.Eval(Container.DataItem,"ContentImageUrl").ToString() %>'>
                        <img src='<%# GetImageFromImageID(DataBinder.Eval(Container.DataItem,"ContentImage").ToString()) %>' alt="">
                        <h3 class="hd">
                            <asp:Literal runat="server" ID="lblTitle" Text='<%# DataBinder.Eval(Container.DataItem,"ContentTitle").ToString() %>'></asp:Literal>
                        </h3>
                    </a>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
