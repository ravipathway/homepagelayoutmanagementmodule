﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContentView.ascx.cs" Inherits="NRNA.Modules.HomePageLayoutManagementModule.Controls.ContentView" %>
<h2>Content</h2>
<asp:Repeater ID="rptItemList" runat="server">
    <HeaderTemplate>
        <div class="news-row-holder">
    </HeaderTemplate>
    <ItemTemplate>
        <div class="news-list">
            <div class="thumbnail">
                <asp:HyperLink ID="linkThumb" runat="server" Target="_blank"
                    NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"ContentImageUrl").ToString() %>'>
                    <asp:Image runat="server" ImageAlign="Left" Style="max-height: 150px; max-width: 150px;"
                ImageUrl='<%# GetImageFromImageID(DataBinder.Eval(Container.DataItem,"ContentImage").ToString()) %>'></asp:Image>
                </asp:HyperLink>
                <div class="meta">
                    <span class="source"><strong>Source: </strong>
                        <asp:HyperLink ID="lblCountry" 
                            NavigateUrl='<%# "http://" + DataBinder.Eval(Container.DataItem,"PostedFromDomain").ToString() %>'
                            Text='<%# DataBinder.Eval(Container.DataItem,"Country").ToString() %>'
                            Target="_blank" runat="server"></asp:HyperLink>
                    </span>
                    <time>
                        <asp:Literal runat="server" Text='<%#  GetFormatedDate(DataBinder.Eval(Container.DataItem,"ContentStartDate").ToString(),
                            DataBinder.Eval(Container.DataItem,"ContentEndDate").ToString())%>'>
                        </asp:Literal>
                    </time>
                </div>
            </div>
            <div class="news-description">
                <h4>
                    <asp:HyperLink ID="lnkTitle" runat="server" CssClass="SubHead" Target="_blank" Text='<%#DataBinder.Eval(Container.DataItem,"ContentTitle").ToString() %>'
                        NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"ContentImageUrl").ToString()%>'>
                    </asp:HyperLink>
                </h4>
                <p>
                    <asp:Literal ID="lblShortDescription" runat="server"
                        Text='<%# "<br />" + DataBinder.Eval(Container.DataItem,"ContentSubTitle").ToString() %>' />
                </p>
                <asp:HyperLink ID="lbkRM" runat="server" CssClass="button" Target="_blank"
                    NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"ContentImageUrl").ToString()%>'>Read More</asp:HyperLink>
            </div>
        </div>
    </ItemTemplate>
    <FooterTemplate>
        </div>
    </FooterTemplate>
</asp:Repeater>

<asp:Literal ID="litPaging" runat="server"></asp:Literal>






<%--<asp:Repeater ID="rptItemList" runat="server">
    <HeaderTemplate>
        <ul class="aside-link-box">
    </HeaderTemplate>
    <ItemTemplate>
        <li>
            <div class="thumb">
                <a href='<%#  GetProjectItemLink()+"/pcid/"+DataBinder.Eval(Container.DataItem,"ProjectId").ToString()%>'
                    visible='<%# ShowShortDescriptionAndThumbNail() %>'>
                    <img src='<%# GetImageFromImageID(DataBinder.Eval(Container.DataItem,"EventThumbnail").ToString()) %>' alt="image decription"></a>
            </div>
            <h4>
                <span class="meta">

                    <asp:Literal runat="server" Text='<%#  GetFormatedDate(DataBinder.Eval(Container.DataItem,"EventStartDateTime").ToString(),DataBinder.Eval(Container.DataItem,"EventEndDateTime").ToString()) +" ,#"+DataBinder.Eval(Container.DataItem,"Country").ToString()%>'></asp:Literal>

                </span>
                <a href='<%# DataBinder.Eval(Container.DataItem,"ContentImageUrl").ToString() %>'>
                    <asp:Literal runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ContentTitle").ToString() %>'></asp:Literal>
                </a>
            </h4>
        </li>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
    </FooterTemplate>
</asp:Repeater>--%>
