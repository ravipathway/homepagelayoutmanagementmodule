﻿using DotNetNuke.Services.Exceptions;
using NRNA.Modules.HomePageLayoutManagementModule.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NRNA.Modules.HomePageLayoutManagementModule.Controls
{
    public partial class TopView : HomePageLayoutManagementModuleModuleBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var tc = new HomePageLayoutController();
                if (PortalId == 0)
                    rptContent.DataSource = tc.GetTopViewContentsForICC(PortalId);
                else
                    rptContent.DataSource = tc.GetTopViewContentsForNCC(PortalId);
                rptContent.DataBind();
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }
    }
}