﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GridView.ascx.cs" Inherits="NRNA.Modules.HomePageLayoutManagementModule.Controls.GridView" %>
<h2 class="sr-only">Services</h2>

<asp:Repeater ID="rptContent" runat="server">
    <HeaderTemplate>
        <ul class="compact-gridlist">
    </HeaderTemplate>
    <ItemTemplate>
        <li class="compact-gridlist-item">
            <div>
                <div class="compact-gridlist-item-img">
                    <asp:HyperLink runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"ContentImageUrl").ToString() %>'>
                        <asp:Image runat="server" Style="max-height: 600px; max-width: 1110px" 
                            ImageUrl='<%# GetImageFromImageID(DataBinder.Eval(Container.DataItem,"ContentImage").ToString()) %>' />
                    </asp:HyperLink>
                </div>
                <div class="compact-gridlist-item-content">
                    <asp:HyperLink runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"ContentImageUrl").ToString() %>'>
                        <h4 class="compact-gridlist-item-content-hd">
                            <asp:Literal runat="server" ID="lblTitle" Text='<%# DataBinder.Eval(Container.DataItem,"ContentTitle").ToString() %>'></asp:Literal>
                        </h4>
                    </asp:HyperLink>
                    <p class="compact-gridlist-item-content-des">
                        <asp:Literal runat="server" ID="Literal1" Text='<%# GetTrimCharacter(DataBinder.Eval(Container.DataItem,"ContentSubTitle").ToString()) %>'></asp:Literal>
                        <a href='<%# DataBinder.Eval(Container.DataItem,"ContentImageUrl").ToString() %>' class="compact-gridlist-item-content-link">more</a>
                    </p>
                </div>
            </div>
        </li>
    </ItemTemplate>
    <FooterTemplate>
        </ul>      
    </FooterTemplate>
</asp:Repeater>
