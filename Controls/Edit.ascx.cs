﻿using DotNetNuke.Services.Exceptions;
using NRNA.Modules.HomePageLayoutManagementModule.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NRNA.Modules.HomePageLayoutManagementModule.Controls
{
    public partial class Edit1 : HomePageLayoutManagementModuleModuleBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                LoadCategory();
                //Implement your edit logic for your module
                if (!Page.IsPostBack)
                {
                    //check if we have an ID passed in via a querystring parameter, if so, load that item to edit.
                    //ItemId is defined in the ItemModuleBase.cs file
                    if (ContentId > 0)
                    {
                        var tc = new HomePageLayoutController();

                        var t = tc.GetContent(ContentId, ModuleId);
                        if (t != null)
                        {
                            txtTitle.Text = t.ContentTitle;
                            txtSubTitle.Text = t.ContentSubTitle;
                            mainImage.FileID = Convert.ToInt32(t.ContentImage);
                            txtImageLink.Text = t.ContentImageUrl;
                            ddlCategory.SelectedValue = t.CategoryId.ToString();

                            //if (t.CategoryId.ToString() == "2")
                            //{
                            //    lblGVPosition.Visible = true;
                            //    ddlGVPosition.Visible = true;
                            //    ddlGVPosition.SelectedValue = t.GridViewPosition.ToString();
                            //}

                            txtStartDate.SelectedDate = Convert.ToDateTime(t.ContentStartDate.ToString());
                            txtEndDate.SelectedDate = Convert.ToDateTime(t.ContentEndDate.ToString());
                            txtBtn1.Text = t.ContentButtonText1;
                            txtLink1.Text = t.ContentLink1;
                            txtBtn2.Text = t.ContentButtonText2;
                            txtLink2.Text = t.ContentLink2;

                            pnlICC.Visible = pnlNCC.Visible = false;

                            if (PortalSettings.UserInfo.IsSuperUser)
                            {
                                pnlICC.Visible = true;

                                if (t.ShowInICCNCC == 3)
                                {
                                    chkToAllNCC.Checked = true;
                                }

                            }
                            else
                            {
                                pnlNCC.Visible = true;
                                if (t.ShowInICCNCC == 1)
                                {
                                    chkRequest.Checked = true;
                                }
                            }
                        }
                    }

                    pnlICC.Visible = pnlNCC.Visible = false;

                    if (PortalSettings.UserInfo.IsSuperUser)
                    {
                        pnlICC.Visible = true;

                    }
                    else
                    {
                        pnlNCC.Visible = true;
                    }
                }
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }

        public void LoadCategory()
        {
            var tc = new HomePageLayoutController();
            ddlCategory.DataSource = (List<Category>)tc.GetCategory();
            ddlCategory.DataTextField = "CategoryName";
            ddlCategory.DataValueField = "CategoryId";
            ddlCategory.DataBind();

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            var t = new HomePageLayout();
            var tc = new HomePageLayoutController();

            if (mainImage.FileID.ToString() == "-1" || mainImage.FileID.ToString() == "")
            {
                lblFileError.Visible = true;
                return;
            }
            //if (ddlCategory.SelectedValue == "2")
            //{
            //    tc.UpdateBeforeInsertInGridView(Convert.ToInt32(ddlGVPosition.SelectedValue), PortalId);
            //}
           

            if (ContentId > 0)
            {
                t = tc.GetContent(ContentId, ModuleId);
            }
            else
            {
                t.CreatedByUserId = UserId;
                t.CreatedOnDate = DateTime.Now;
            }

            t.ContentTitle = txtTitle.Text;
            t.ContentSubTitle = txtSubTitle.Text;

            t.CategoryId = Convert.ToInt32(ddlCategory.SelectedValue);
          //  t.GridViewPosition = Convert.ToInt32(ddlGVPosition.SelectedValue);

            t.ContentImage = mainImage.FileID.ToString();
            t.ContentImageUrl = txtImageLink.Text.Trim();
            if (txtStartDate.SelectedDate != null)
                t.ContentStartDate = txtStartDate.SelectedDate.Value;
            else
                t.ContentStartDate = DateTime.Now.AddDays(-2);

            if (txtEndDate.SelectedDate != null)
                t.ContentEndDate = txtEndDate.SelectedDate.Value;
            else
                t.ContentEndDate = DateTime.Now.AddYears(10);

            t.ContentButtonText1 = txtBtn1.Text;
            t.ContentLink1 = txtLink1.Text;
            t.ContentButtonText2 = txtBtn2.Text;
            t.ContentLink2 = txtLink2.Text;
            t.LastModifiedByUserId = UserId;
            t.LastModifiedOnDate = DateTime.Now;
            t.ModuleId = ModuleId;
            t.PortalId = PortalId;
            t.PostedFromDomain = PortalAlias.HTTPAlias;
            t.Country = PortalSettings.PortalName;
            t.ShowInICCNCC = 0;

            if (pnlNCC.Visible && chkRequest.Checked)
            {
                t.ShowInICCNCC = 1;
            }
            else if (pnlICC.Visible && chkToAllNCC.Checked)
            {
                t.ShowInICCNCC = 3;
            }

            if (ContentId > 0)
            {
                tc.UpdateItem(t);
            }
            else
            {
                tc.CreateItem(t);
            }
            // Response.Redirect(DotNetNuke.Common.Globals.NavigateURL());
            Response.Redirect("/manage-layout-content" + "?UpdateSuccess=true");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(DotNetNuke.Common.Globals.NavigateURL());
        }

        //protected void ddlCategory_SelectedIndexChanged1(object sender, EventArgs e)
        //{
        //    if (ddlCategory.SelectedValue == "2")
        //    {
        //        lblGVPosition.Visible = true;
        //        ddlGVPosition.Visible = true;
        //    }
        //    else
        //    {
        //        lblGVPosition.Visible = false;
        //        ddlGVPosition.Visible = false;
        //    }
        //}
    }
}