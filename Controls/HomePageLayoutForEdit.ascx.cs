﻿using DotNetNuke.Services.Exceptions;
using DotNetNuke.UI.Utilities;
using NRNA.Modules.HomePageLayoutManagementModule.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NRNA.Modules.HomePageLayoutManagementModule.Controls
{
    public partial class HomePageLayoutForEdit : HomePageLayoutManagementModuleModuleBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.QueryString["UpdateSuccess"] == "true")
            {
                pnlMsg.CssClass = "dnnFormMessage dnnFormSuccess";
                litMessage.Text = "Data Saved successfully!!";
            }

            try
            {
                bindGV();
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }

        private void bindGV()
        {
            var tc = new HomePageLayoutController();
            if (PortalId == 0)
                gvFC.DataSource = tc.GetContentsForICC(PortalId);
            else
                gvFC.DataSource = tc.GetContentsForNCC(PortalId);
            gvFC.DataBind();
        }

        protected void gvFC_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                var tc = new HomePageLayoutController();
                tc.DeleteContent(Convert.ToInt32(e.CommandArgument));
            }
        }

        protected void gvFC_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lnkDelete = e.Row.FindControl("lnkDelete") as LinkButton;
                var t = (HomePageLayout)e.Row.DataItem;

                lnkDelete.CommandArgument = t.ContentId.ToString();
                lnkDelete.Enabled = lnkDelete.Visible = true;

                ClientAPI.AddButtonConfirm(lnkDelete, "Are you sure you want to delete?");
            }
        }

        protected void gvFC_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvFC.PageIndex = e.NewPageIndex;
            bindGV();
        }

        protected void gvFC_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            bindGV();
        }


        protected void gv_Sorting(object sender, GridViewSortEventArgs e)
        {
            // e.SortDirection = SortDirection.Descending;
            var tc = new HomePageLayoutController();
            SortDirection sortDirection = SortDirection.Ascending;
            string sortField = string.Empty;


            SortGridview((System.Web.UI.WebControls.GridView)sender, e, out sortDirection, out sortField);
            string strSortDirection = sortDirection == SortDirection.Ascending ? "ASC" : "DESC";
            gvFC.DataSource = tc.GetHomePageItemsforAdmin(PortalId, e.SortExpression, strSortDirection);
            gvFC.DataBind();
        }
        private void SortGridview (System.Web.UI.WebControls.GridView gridView, GridViewSortEventArgs e, out SortDirection sortDirection, out string sortField)
        {
            sortField = e.SortExpression;
            sortDirection = e.SortDirection;
            if (gridView.Attributes["CurrentSortField"] != null && gridView.Attributes["CurrentSortDirection"] != null)
            {
                if (sortField == gridView.Attributes["CurrentSortField"])
                {
                    if (gridView.Attributes["CurrentSortDirection"] == "ASC")
                    {
                        sortDirection = SortDirection.Descending;
                    }
                    else
                    {
                        sortDirection = SortDirection.Ascending;
                    }
                }
                gridView.Attributes["CurrentSortField"] = sortField;
                gridView.Attributes["CurrentSortDirection"] = (sortDirection == SortDirection.Ascending ? "ASC" : "DESC");
            }
        }

        protected void Search(object sender, EventArgs e)
        {
            string condition = "";
            var tc = new HomePageLayoutController();

            if (txtSearch.Text.Trim().Length > 0)
            {
                var searchText = txtSearch.Text.TrimEnd();
                searchText = searchText.TrimStart();

                //condition += "WHERE (ContentTitle like N'%" + searchText + "%' or ContentSubTitle like N'%" + searchText + "%')";
                gvFC.DataSource = tc.GetSearchedContent(searchText, PortalId);
            }
            else
            {
                if (PortalId == 0)
                    gvFC.DataSource = tc.GetContentsForICC(PortalId);
                else
                    gvFC.DataSource = tc.GetContentsForNCC(PortalId);
                
            }
            gvFC.DataBind();
        }
    }
}